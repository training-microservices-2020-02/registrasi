# Aplikasi Registrasi Pelatihan #

Cara menjalankan :

1. Update repo `konfigurasi` untuk mendapatkan konfigurasi untuk aplikasi `registrasi`

2. Jalankan `configserver` dan `discoveryserver`

3. Buat user database postgresql

    ```
    createuser -P registrasiapp
    Password : registrasi123
    ```

4. Buat database `registrasidb`

    ```
    createdb -Oregistrasiapp registrasidb
    ```

5. Start aplikasi

    ```
    mvn clean spring-boot:run
    ```

6. Isi sample data dari file `src/test/resources/sql/sample-data.sql`

7. Akses ke aplikasi di endpoint `http://localhost:8080/api/materi/`

    ```
    curl http://localhost:8080/api/materi/
    ```
   
    Hasilnya seperti ini
    
    ```json
    [ {
      "id" : "m001",
      "kode" : "MSA-001",
      "nama" : "Microservices Architecture",
      "durasi" : 15
    }, {
      "id" : "s001",
      "kode" : "SPR-001",
      "nama" : "Spring Boot",
      "durasi" : 40
    } ]
    ```