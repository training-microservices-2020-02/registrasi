package id.artivisi.training.microservice.registrasi.registrasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrasiApplication.class, args);
	}

}
