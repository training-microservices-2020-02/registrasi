package id.artivisi.training.microservice.registrasi.registrasi.controller;

import id.artivisi.training.microservice.registrasi.registrasi.dao.MateriPelatihanDao;
import id.artivisi.training.microservice.registrasi.registrasi.entity.MateriPelatihan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/materi")
public class MateriPelatihanController {

    @Autowired private MateriPelatihanDao materiPelatihanDao;

    @GetMapping("/")
    public Iterable<MateriPelatihan> daftarMateri() {
        return materiPelatihanDao.findAll();
    }
}
