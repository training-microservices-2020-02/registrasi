package id.artivisi.training.microservice.registrasi.registrasi.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data @Entity
public class Kelas {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @NotEmpty
    private String kode;

    @NotNull @NotEmpty
    private String nama;

    @NotNull
    private LocalDate tanggalMulai;

    @NotNull
    private LocalDate tanggalSelesai;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_materi_pelatihan")
    private MateriPelatihan materiPelatihan;

    @ManyToMany
    @JoinTable(
            name = "peserta_kelas",
            joinColumns = {@JoinColumn(name = "id_kelas")},
            inverseJoinColumns = {@JoinColumn(name = "id_peserta")})
    private Set<Peserta> daftarPeserta = new HashSet<>();

}
