package id.artivisi.training.microservice.registrasi.registrasi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class BackendInfoController {

    @GetMapping("/api/backend/")
    public Map<String, Object> backendInfo(HttpServletRequest request) {
        Map<String, Object> info = new HashMap<>();
        info.put("host", request.getLocalName());
        info.put("port", request.getLocalPort());
        info.put("ip", request.getLocalAddr());
        return info;
    }
}
