package id.artivisi.training.microservice.registrasi.registrasi.dao;

import id.artivisi.training.microservice.registrasi.registrasi.entity.MateriPelatihan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MateriPelatihanDao extends PagingAndSortingRepository<MateriPelatihan, String> {
}
