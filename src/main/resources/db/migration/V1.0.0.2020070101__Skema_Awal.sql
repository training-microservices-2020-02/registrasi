create table materi_pelatihan (
    id varchar(36),
    kode varchar(100) not null,
    nama varchar(255) not null,
    durasi int not null,
    primary key (id),
    unique (kode)
);

create table peserta (
    id varchar(36),
    email varchar(100) not null,
    nama varchar(255) not null,
    no_hp varchar(100) not null,
    primary key (id),
    unique (email),
    unique (no_hp)
);

create table kelas (
    id varchar(36),
    kode varchar(100) not null,
    nama varchar(255) not null,
    tanggal_mulai date not null ,
    tanggal_selesai date not null,
    id_materi_pelatihan varchar(36) not null,
    primary key (id),
    unique (kode),
    foreign key (id_materi_pelatihan) references materi_pelatihan(id)
);

create table peserta_kelas (
    id_kelas varchar(36),
    id_peserta varchar(36),
    primary key (id_kelas, id_peserta),
    foreign key (id_kelas) references kelas(id),
    foreign key (id_peserta) references peserta(id)
);